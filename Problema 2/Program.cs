﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Problema_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int nr = int.Parse(Console.ReadLine());
            int[] numere = new int[nr];

            for (int i = 0; i < nr; i++)
                numere[i] = int.Parse(Console.ReadLine());

            int k = 0;
            int[] elem_gasite = new int[0];

            int a = int.Parse(Console.ReadLine());

            for (int i = 0; i < nr; i++)
                if (numere[i] < a)
                {
                    Array.Resize(ref elem_gasite, elem_gasite.Length + 1);
                    elem_gasite[k] = numere[i];
                    k++;
                }
            Console.WriteLine("Numarul elementelor mai mici decat a este: " + k);
            for (int i = 0; i < k; i++)
            {
                Console.Write(elem_gasite[i] + " ");
            }


            Console.ReadKey();
        }
    }
}